﻿using System;
using System.Threading;
using CallBackSystem.Presenters;
using System.Collections.Generic;

/***
 * Create by JohnyKO 17.11.2018 
 ***/

namespace CallBackSystem.Models
{
    class DataBaseManager
    {
        private static List<CustomCallBack> subscribers = new List<CustomCallBack>();

        public static void Subscribe(CustomCallBack callBack)
        {
            subscribers.Add(callBack);
        }

        public static void SignInWithDB(string Login, 
                                        string Password)
        {
            Thread.Sleep(1000);
            foreach(CustomCallBack subcribe in subscribers)
                subcribe.CallingBack(true);
        }
    }
}

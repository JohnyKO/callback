﻿using System;

/***
 * Create by JohnyKO 17.11.2018 
 ***/

namespace CallBackSystem.Presenters
{
    interface CustomCallBack
    {
        void CallingBack(bool answer);
    }
}

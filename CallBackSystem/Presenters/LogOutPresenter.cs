﻿using CallBackSystem.Source;
using CallBackSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/***
 * Create by JohnyKO 1.12.2018 
 ***/

namespace CallBackSystem.Presenters
{
    class LogOutPresenter : CustomCallBack
    {
        public LogOutPresenter()
        {
            DataBaseManager.Subscribe(this);
        }

        public void CallingBack(bool answer)
        {
            MainClass.DisplayData($"Returned answer : {answer.ToString()} from LogOutPresenter");
        }
    }
}

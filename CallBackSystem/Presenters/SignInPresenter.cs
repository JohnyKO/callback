﻿using System;
using CallBackSystem.Source;
using CallBackSystem.Models;

/***
 * Create by JohnyKO 17.11.2018 
 ***/

namespace CallBackSystem.Presenters
{
    class SignInPresenter : CustomCallBack
    {

        public SignInPresenter()
        {
            DataBaseManager.Subscribe(this);
        }

        public void CallingBack(bool answer)
        {
            MainClass.DisplayData($"Returned answer : {answer.ToString()} from SignInPresenter");  
        }
    }
}

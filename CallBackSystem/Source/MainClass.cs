﻿using System;
using System.Threading;
using CallBackSystem.Models;
using CallBackSystem.Presenters;

namespace CallBackSystem.Source
{
/***
 * Create by JohnyKO 17.11.2018 
 ***/

    class MainClass
    {
        public static void Main()
        {
            Console.WriteLine("Hello!");
            SignInPresenter presenter1 = new SignInPresenter();
            SignUpPresenter presenter2 = new SignUpPresenter();
            LogOutPresenter presenter3 = new LogOutPresenter();

            var Login = Console.ReadLine();
            var Password = Console.ReadLine();

            DataBaseManager.SignInWithDB(Login, Password);
            Console.ReadLine();
        }

        public static void DisplayData(string data)
        {
            Console.WriteLine(data);
            Console.ReadLine();
        }
    }
}
